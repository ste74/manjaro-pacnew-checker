��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     �  -   �  '   �  9      '   :  �  b  !   Z	  D   |	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>znaleziono nowe pliki pacnew/pacsave:\n</span>
pamiętaj:\n
musisz być świadomy swoich wyborów. jeśli nie jesteś pewien, zapytaj za pomocą naszych kanałów społecznościowych, takich jak nasze forum wsparcia. Nie rób nic Zachowaj oryginalny plik i usuń plik $suffix Zarządzanie pacnew/pacsave zakończone Teraz <span foreground='red'>usuniemy</span> plik $suffix Zastąp oryginalny plik plikiem $suffix Ten program jest darmowym oprogramowaniem, możesz go rozpowszechniać i/lub modyfikować zgodnie z warunkami licencji GNU General Public License opublikowanej przez Fundację Wolnego Oprogramowania w wersji 3.

Ten program jest rozpowszechniany w nadziei, że będzie on użyteczny, ale bez żadnej gwarancji, nawet domyślnej gwarancji handlowej lub przydatności do określonego celu. Zobacz licencję GNU General Public License dla więcej szczegółów.

Autor: Stefano Capitani
stefano@manjaro.org Wyświetl i połącz plik $suffix Co chcesz zrobić z plikiem <span foreground='red'>$basefile</span>? 