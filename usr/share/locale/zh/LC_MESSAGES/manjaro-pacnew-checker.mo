��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     ^  +   n      �  B   �  "   �  p  !     �  E   �   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 发现新的pacnew/pacsave文件：\n
请记住：\n
您必须意识到自己的选择。如果您不确定，请通过我们的社交渠道，如支持论坛，进行咨询。 什么也不做 保留原始文件，删除$suffix文件。 pacnew/pacsave的管理已完成 现在我们将<span foreground='red'>删除</span> $suffix 文件 用$suffix文件替换原始文件 这个程序是免费软件，您可以根据GNU通用公共许可证的条款重新分发或修改它，该许可证由自由软件基金会发布，版本3。

这个程序是希望它有用，但没有任何保证，甚至没有适销性或特定用途的暗示保证。更多细节请参阅GNU通用公共许可证。

作者：Stefano Capitani
stefano@manjaro.org 查看并合并$suffix文件 你想要对文件<span foreground='red'>$basefile</span>做什么？ 