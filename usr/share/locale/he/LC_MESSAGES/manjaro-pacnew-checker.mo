��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  ?  �     �  ;     5   @  '   v  2   �  ;  �  '   
  X   5
   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>נמצאו קבצי pacnew/pacsave חדשים:\n</span>
שימו לב:\n
עליכם להיות מודעים לבחירותיכם. אם אינכם בטוחים, אנא פנו אלינו באמצעות ערוצי התקשורת החברתיים שלנו כגון הפורום התמיכה שלנו. אל תעשה כלום שמור את המקורי והסר את הקובץ $suffix ניהול הסיום של pacnew/pacsave הושלם. כעת נסיר את הקובץ $suffix החלף את המקורי עם הקובץ $suffix תוכנה זו היא תוכנה חופשית, ניתן להפצתה ו/או לשינוייה תחת תנאי הרישיון הציבורי הכללי של קרן התוכנה החופשית, גרסה 3 של הרישיון.

התוכנה מופצת בתקווה שתהיה שימושית, אך ללא כל אחריות, גם ללא אחריות משתמש או תאימות למטרה מסוימת. עיין ברישיון הציבורי הכללי של קרן התוכנה החופשית לפרטים נוספים.

מחבר: סטפנו קפיטני
stefano@manjaro.org צפה ומזג את הקובץ $suffix מה אתה רוצה לעשות עם הקובץ <span foreground='red'>$basefile</span>? 