��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     �  .   �  !   �  G   �  #   .  �  R  "   :	  P   ]	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>faili mpya ya pacnew/pacsave imepatikana:\n</span
kumbuka:\n
unapaswa kuwa makini na chaguo zako. ikiwa haujiamini tafadhali uliza kupitia njia zetu za kijamii kama vile jukwaa letu la msaada. Ära tee midagi Simpanlahkan yang asli dan hapus file $suffix. Pengurusan pacnew/pacsave selesai Nú munum við <span foreground='red'>fjarlægja</span> skrána $suffix Ganti yang asli dengan file $suffix Program ini adalah perangkat lunak bebas, Anda dapat mendistribusikannya dan/atau memodifikasinya sesuai dengan ketentuan lisensi umum gnu yang diterbitkan oleh yayasan perangkat lunak bebas versi 3 dari lisensi tersebut.

Program ini didistribusikan dengan harapan akan bermanfaat, namun tanpa jaminan apapun, tanpa bahkan jaminan tersirat akan kelayakan atau kesesuaian untuk tujuan tertentu. Lihat lisensi umum gnu untuk lebih detailnya.

Penulis: Stefano Capitani
stefano@manjaro.org Visa och slå samman filen $suffix Apa yang ingin Anda lakukan dengan file <span foreground='red'>$basefile</span>? 