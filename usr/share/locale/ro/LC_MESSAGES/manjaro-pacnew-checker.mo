��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     �  7   �  $   �  @     ,   M  �  z  *   y	  L   �	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Fișierele noi pacnew/pacsave au fost găsite:\n
Țineți minte:\n
Trebuie să fiți conștienți de alegerile voastre. Dacă nu sunteți siguri, vă rugăm să întrebați folosind canalele noastre sociale, cum ar fi forumul nostru de suport. Nu face nimic Păstrați originalul și eliminați fișierul $suffix. Gestionarea pacnew/pacsave completă Acum vom <span foreground='red'>șterge</span> fișierul $suffix Înlocuiți originalul cu fișierul $suffix. Acest program este un software liber, pe care îl puteți redistribui și/sau modifica sub termenii licenței generale publice GNU, așa cum este publicată de Fundația pentru Software Liber, versiunea 3 a licenței.

Acest program este distribuit în speranța că va fi util, dar fără nicio garanție, fără chiar garanția implicită de comercializare sau potrivire pentru un anumit scop. Consultați licența generală publică GNU pentru mai multe detalii.

Autor: Stefano Capitani
stefano@manjaro.org Vizualizați și uniți fișierul $suffix. Ce doriți să faceți cu fișierul <span foreground='red'>$basefile</span>? 