��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     s  /   �  *   �  >   �  $     �  A      -	  D   N	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>nye pacnew/pacsave filer funnet:\n</span>
husk:\n
du må være klar over dine valg. hvis du er usikker, vennligst spør gjennom våre sosiale kanaler som vårt support forum. Ikke gjør noe Behold den opprinnelige og fjern $suffix filen. Administrasjon av pacnew/pacsave fullført Nå skal vi <span foreground='red'>fjerne</span> filen $suffix Erstatt originalen med filen $suffix Dette programmet er gratis programvare, du kan redistribuere det og/eller endre det under vilkårene til GNU General Public License som publisert av Free Software Foundation, versjon 3 av lisensen.

Dette programmet distribueres i håp om at det vil være nyttig, men uten noen form for garanti, uten engang den underforståtte garantien for salgbarhet eller egnethet for et bestemt formål. Se GNU General Public License for mer informasjon.

Forfatter: Stefano Capitani
stefano@manjaro.org Vis og slå sammen $suffix-filen Hva vil du gjøre med filen <span foreground='red'>$basefile</span>? 