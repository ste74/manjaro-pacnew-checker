��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �  �     /  O   L  C   �  Q   �  E   2  �  x  ?   (  f   h   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Намерени са нови pacnew/pacsave файлове:\n</span>
Имайте предвид:\n
Трябва да сте внимателни с изборите си. Ако не сте сигурни, моля попитайте чрез нашите социални канали, като например нашия форум за поддръжка. Не правете нищо Запазете оригинала и премахнете файла $suffix. Управлението на pacnew/pacsave е завършено. Сега ще <span foreground='red'>премахнем</span> файла $suffix Заменете оригиналния файл с $suffix файл. Този програмен продукт е безплатен софтуер, който можете да разпространявате и/или променяте съгласно условията на GNU Обществената лиценз на свободен софтуер, както е публикувана от Фондацията за свободен софтуер в нейната трета версия.

Този програмен продукт се разпространява с надеждата, че ще бъде полезен, но без никакви гаранции, дори без подразбиращата гаранция за търговска пригодност или пригодност за конкретна цел. Вижте GNU Обществената лиценз за повече подробности.

Автор: Стефано Капитани
stefano@manjaro.org Прегледайте и обединете файла $suffix Какво искате да направите с файла <span foreground='red'>$basefile</span>? 