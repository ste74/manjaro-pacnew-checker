��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     �  /   �  %   �     �  ,     �  E  #   =	  C   a	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>nuovi file pacnew/pacsave trovati:\n</span>
tieni presente:\n
devi essere consapevole delle tue scelte. se non sei sicuro, per favore chiedi tramite i nostri canali social come il nostro forum di supporto. Non fare niente Mantieni l'originale e rimuovi il file $suffix. Gestione di pacnew/pacsave completata Ora rimuoveremo il file $suffix Sostituisci l'originale con il file $suffix. Questo programma è un software libero, puoi ridistribuirlo e/o modificarlo secondo i termini della licenza pubblica generale gnu pubblicata dalla fondazione software libero versione 3 della licenza.

Questo programma è distribuito nella speranza che possa essere utile, ma senza alcuna garanzia, senza nemmeno la garanzia implicita di commerciabilità o idoneità per uno scopo particolare. Consulta la licenza pubblica generale gnu per maggiori dettagli.

Autore: Stefano Capitani
stefano@manjaro.org Visualizza e unisci il file $suffix Cosa vuoi fare con il file <span foreground='red'>$basefile</span>? 