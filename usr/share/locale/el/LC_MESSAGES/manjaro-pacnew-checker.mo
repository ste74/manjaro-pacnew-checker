��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �  �      D  q   e  P   �  \   (  [   �  �  �  H   u  c   �   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Νέα αρχεία pacnew/pacsave βρέθηκαν:
Να έχετε υπόψη σας:
Πρέπει να είστε ενήμεροι για τις επιλογές σας. Εάν δεν είστε σίγουροι, παρακαλούμε να επικοινωνήσετε μαζί μας μέσω των κοινωνικών μας δικτύων, όπως το φόρουμ υποστήριξης μας. Μην κάνεις τίποτα Διατηρήστε το αρχικό αρχείο και αφαιρέστε το αρχείο με το $suffix Ολοκλήρωση διαχείρισης των αρχείων pacnew/pacsave Τώρα θα <span foreground='red'>αφαιρέσουμε</span> το αρχείο $suffix Αντικαταστήστε το αρχικό αρχείο με το αρχείο $suffix. Αυτό το πρόγραμμα είναι ελεύθερο λογισμικό, μπορείτε να το διανέμετε και/ή να το τροποποιείτε υπό τους όρους της γενικής δημόσιας άδειας GNU όπως δημοσιεύτηκε από το Ίδρυμα Ελεύθερου Λογισμικού έκδοση 3 της άδειας.

Αυτό το πρόγραμμα διανέμεται με την ελπίδα ότι θα είναι χρήσιμο, αλλά χωρίς καμία εγγύηση, ούτε καν την υπονοούμενη εγγύηση εμπορικότητας ή καταλληλότητας για συγκεκριμένο σκοπό. Δείτε τη γενική δημόσια άδεια GNU για περισσότερες λεπτομέρειες.

Συγγραφέας: Στέφανος Καπιτάνης
stefano@manjaro.org Προβολή και συγχώνευση του αρχείου $suffix Τι θέλετε να κάνετε με το αρχείο <span foreground='red'>$basefile</span>; 