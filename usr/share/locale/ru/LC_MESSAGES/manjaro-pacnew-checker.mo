��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �  �      /  G   P  6   �  0   �  N      �  O  ?   O  `   �   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>обнаружены новые файлы pacnew/pacsave:\n</span>
имейте в виду:\n
вы должны быть в курсе своих выборов. если вы не уверены, пожалуйста, обратитесь к нам через наши социальные каналы, такие как наш форум поддержки. Ничего не делайте Сохраните оригинал и удалите файл $suffix. Управление pacnew/pacsave завершено Теперь мы удалим файл $suffix. Замените оригинал на файл с суффиксом $suffix. Эта программа является бесплатным программным обеспечением, вы можете распространять его и/или изменять его в соответствии с условиями общественной лицензии GNU, опубликованной Фондом свободного программного обеспечения, версия 3 лицензии.

Эта программа распространяется в надежде на то, что она будет полезна, но без каких-либо гарантий, даже без подразумеваемой гарантии товарной пригодности или пригодности для конкретной цели. См. общественную лицензию GNU для получения дополнительной информации.

Автор: Стефано Капитани
Электронная почта: stefano@manjaro.org Просмотреть и объединить файл $suffix Что вы хотите сделать с файлом <span foreground='red'>$basefile</span>? 