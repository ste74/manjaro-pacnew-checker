��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     �  F   �  $   �  C     2   U  �  �  '   W	  J   	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>novi pacnew/pacsave datoteke pronađene:\n</span>
imajte na umu:\n
morate biti svjesni svojih izbora. ako niste sigurni, molimo vas da se obratite našim društvenim kanalima poput našeg foruma za podršku. Ne radite ništa Zadržite originalnu datoteku i uklonite datoteku s nastavkom $suffix. Upravljanje pacnew/pacsave završeno Sada ćemo <span foreground='red'>ukloniti</span> datoteku $suffix. Zamijenite originalnu datoteku s datotekom $suffix Ovaj program je besplatni softver, možete ga redistribuirati i/ili mijenjati pod uvjetima GNU opće javne licence objavljene od strane Zaklade za slobodni softver u verziji 3 licence.

Ovaj program se distribuira u nadi da će biti koristan, ali bez ikakvih jamstava, čak ni podrazumijevanih jamstava o tržišnoj vrijednosti ili prikladnosti za određenu svrhu. Pogledajte GNU opću javnu licencu za više detalja.

Autor: Stefano Capitani
stefano@manjaro.org Pregledajte i spojite datoteku $suffix. Što želite učiniti s datotekom <span foreground='red'>$basefile</span>? 