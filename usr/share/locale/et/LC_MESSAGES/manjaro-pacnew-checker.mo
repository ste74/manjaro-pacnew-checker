��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     �  +   �  !   �     �  "     �  '     	  A    	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>uued pacnew/pacsave failid leitud:\n</span>
pea meeles:\n
pead olema teadlik oma valikutest. kui sa pole kindel, siis küsi nõu meie sotsiaalmeedia kanalite, näiteks meie toetusfoorumi kaudu. Ära tee midagi Säilita originaal ja eemalda $suffix fail. Pacnew/pacsave täielik haldamine Nüüd eemaldame $suffix faili. Asenda algne fail $suffix failiga. See programm on tasuta tarkvara, mida saate levitada ja/või muuta vastavalt GNU üldise avaliku litsentsi tingimustele, mille on avaldanud vaba tarkvara sihtasutus. Litsentsi kolmas versioon.

See programm levitatakse lootuses, et see on kasulik, kuid ilma igasuguse garantiita, isegi ilma kaudse garantiita müügikõlblikkuse või konkreetse eesmärgi jaoks. Vaadake GNU üldist avalikku litsentsi täpsemate tingimuste kohta.

Autor: Stefano Capitani
stefano@manjaro.org Vaata ja ühenda $suffix fail Mida soovid teha failiga <span foreground='red'>$basefile</span>? 