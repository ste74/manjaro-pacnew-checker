��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     y  .   �  '   �  <   �  +     �  H     /	  D   O	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>nye pacnew/pacsave filer fundet:\n</span>
husk:\n
du skal være opmærksom på dine valg. hvis du er i tvivl, bedes du spørge via vores sociale kanaler, såsom vores supportforum. Gør ingenting Behold den oprindelige og fjern $suffix filen. Håndtering af pacnew/pacsave fuldført Nu vil vi <span foreground='red'>fjerne</span> $suffix filen Erstat den originale fil med filen $suffix. Dette program er gratis software, du kan redistribuere det og/eller ændre det under betingelserne i GNU General Public License, som er udgivet af Free Software Foundation i version 3 af licensen.

Dette program distribueres i håb om, at det vil være nyttigt, men uden nogen form for garanti, uden endda den underforståede garanti for salgbarhed eller egnethed til et bestemt formål. Se GNU General Public License for flere detaljer.

Forfatter: Stefano Capitani
stefano@manjaro.org Vis og sammenflet $suffix filen Hvad vil du gøre med filen <span foreground='red'>$basefile</span>? 