��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �    �     �  8   �  "   �        .   >  �  m  "   L	  D   o	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>nájdené nové súbory pacnew/pacsave:\n</span>
pamätajte si:\n
musíte byť si vedomí svojich možností. ak nie ste si istí, prosím informujte sa prostredníctvom našich sociálnych kanálov, ako napríklad náš podporný fórum. Nerobiť nič Zachovajte pôvodný súbor a odstráňte súbor $suffix Správa pacnew/pacsave dokončená Teraz odstránime súbor $suffix Nahradiť pôvodný súbor s $suffix súborom. Tento program je voľný softvér, môžete ho redistribuovať a/alebo upravovať podľa podmienok licencie GNU General Public License, ktorú vydala nadácia pre voľný softvér vo verzii 3.

Tento program sa distribuuje v nádeji, že bude užitočný, ale bez akejkoľvek záruky, ani implicitnej záruky obchodovateľnosti alebo vhodnosti pre konkrétny účel. Pre viac informácií pozrite licenciu GNU General Public License.

Autor: Stefano Capitani
stefano@manjaro.org Zobraziť a spojiť súbor $suffix Čo chcete urobiť súborom <span foreground='red'>$basefile</span>? 