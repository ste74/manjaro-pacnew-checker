��    
      l      �       �   �   �   
   �  -   �  %   �  A     *   ]  �  �     a  O   �    �  �   �     �  3   �  "   �       B   +  �  n  %   j	  H   �	                               	      
        <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Uudet pacnew/pacsave-tiedostot löytyivät:
muista:
sinun täytyy olla tietoinen valinnoistasi. Jos olet epävarma, kysy lisätietoja sosiaalisen median kanavissamme, kuten tukifoorumillamme. Älä tee mitään Säilytä alkuperäinen ja poista $suffix-tiedosto. Pacnew/pacsave-hallinnointi valmis Nyt poistamme $suffix-tiedoston Korvaa alkuperäinen $suffix-tiedosto alkuperäisellä tiedostolla Tämä ohjelma on ilmaista ohjelmistoa, ja sitä saa levittää ja/tai muokata gnu yleisen julkisen lisenssin ehtojen mukaisesti, jotka on julkaissut ilmaisen ohjelmiston säätiö lisenssin version 3 mukaisesti.

Tämä ohjelma jaetaan toivossa, että siitä olisi hyötyä, mutta ilman mitään takuuta, ei edes epäsuoraa takuuta kaupallisesta soveltuvuudesta tai tiettyyn tarkoitukseen sopivuudesta. Katso gnu yleinen julkinen lisenssi lisätietoja varten.

Tekijä: Stefano Capitani
stefano@manjaro.org Näytä ja yhdistä $suffix-tiedosto. Mitä haluat tehdä tiedostolle <span foreground='red'>$basefile</span>? 