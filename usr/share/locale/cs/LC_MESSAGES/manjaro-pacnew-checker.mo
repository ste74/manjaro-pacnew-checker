��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     �  0   �  !   �      �  ,     �  L  "   D	  C   g	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>nové soubory pacnew/pacsave nalezeny:\n</span>
mějte na paměti:\n
musíte být si vědomi svých volb. pokud si nejste jisti, obraťte se na nás pomocí našich sociálních kanálů, jako je náš fórum podpory. Nedělej nic Zachovejte původní a odstraňte soubor $suffix Správa pacnew/pacsave dokončena Nyní odstraníme soubor $suffix Nahraďte původní soubor souborem $suffix. Tento program je volný software, můžete jej redistribuovat a/nebo upravovat podle podmínek GNU General Public License, jak je publikována nadací Free Software Foundation ve verzi 3 této licence.

Tento program je distribuován v naději, že bude užitečný, ale bez jakéhokoli záručního ujednání, ani implicitní záruky obchodovatelnosti nebo vhodnosti pro konkrétní účel. Pro více informací se podívejte na GNU General Public License.

Autor: Stefano Capitani
stefano@manjaro.org Zobrazit a sloučit soubor $suffix Co chcete udělat souborem <span foreground='red'>$basefile</span>? 