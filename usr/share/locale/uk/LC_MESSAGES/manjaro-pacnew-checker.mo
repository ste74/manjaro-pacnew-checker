��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �  �     3  _   R  5   �  1   �  K     �  f  A   �  ^   6   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>знайдено нові файли pacnew/pacsave:\n</span>
пам'ятайте:\n
ви повинні бути усвідомлені своїх виборів. якщо ви не впевнені, будь ласка, запитайте за допомогою наших соціальних каналів, таких як наш форум підтримки. Нічого не робіть Збережіть оригінал та видаліть файл з суфіксом $suffix. Керування pacnew/pacsave завершено. Тепер ми видалимо файл $suffix Замініть оригінал на файл з суфіксом $suffix Ця програма є безкоштовним програмним забезпеченням, ви можете розповсюджувати його та/або змінювати його за умовами загальної публічної ліцензії GNU, опублікованої Фондом вільного програмного забезпечення версії 3 ліцензії.

Ця програма поширюється з надією, що вона буде корисною, але без будь-яких гарантій, навіть неявних гарантій товарної якості або придатності для певної мети. Для отримання додаткової інформації дивіться загальну публічну ліцензію GNU.

Автор: Стефано Капітані
stefano@manjaro.org Переглянути та об'єднати файл з $suffix Що ви хочете зробити з файлом <span foreground='red'>$basefile</span>? 