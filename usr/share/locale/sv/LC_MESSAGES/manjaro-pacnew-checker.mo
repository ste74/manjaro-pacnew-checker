��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     v  3   �  $   �  E   �  '   $  �  L  !   7	  D   Y	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>nya pacnew/pacsave filer hittades:\n</span>
tänk på:\n
du måste vara medveten om dina val. om du är osäker, fråga gärna via våra sociala kanaler, som vårt supportforum. Gör ingenting Behåll originalfilen och ta bort filen med $suffix Hantering av pacnew/pacsave komplett Nu kommer vi att <span foreground='red'>ta bort</span> filen $suffix. Ersätt originalfilen med filen $suffix Detta program är fri programvara, du kan distribuera det och/eller modifiera det under villkoren i GNU General Public License som publicerats av Free Software Foundation, version 3 av licensen.

Detta program distribueras i hopp om att det ska vara användbart, men utan någon garanti, utan ens den underförstådda garantin för säljbarhet eller lämplighet för ett specifikt syfte. Se GNU General Public License för mer information.

Författare: Stefano Capitani
stefano@manjaro.org Visa och sammanfoga filen $suffix Vad vill du göra med filen <span foreground='red'>$basefile</span>? 