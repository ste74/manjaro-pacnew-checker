��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     �  /   �  "   �  C     *   E  �  p  &   `	  M   �	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>novos arquivos pacnew/pacsave encontrados:\n</span>
tenha em mente:\n
você deve estar ciente de suas escolhas. se estiver inseguro, por favor, pergunte através de nossos canais sociais, como nosso fórum de suporte. Não faça nada Mantenha o original e remova o arquivo $suffix. Gestão de pacnew/pacsave completa Agora vamos <span foreground='red'>remover</span> o arquivo $suffix Substitua o original pelo arquivo $suffix. Este programa é um software livre, você pode redistribuí-lo e/ou modificá-lo sob os termos da licença pública geral GNU, publicada pela Fundação de Software Livre na versão 3 da licença.

Este programa é distribuído na esperança de que seja útil, mas sem qualquer garantia, sem mesmo a garantia implícita de comercialização ou adequação a uma finalidade específica. Consulte a licença pública geral GNU para obter mais detalhes.

Autor: Stefano Capitani
stefano@manjaro.org Visualizar e mesclar o arquivo $suffix O que você quer fazer com o arquivo <span foreground='red'>$basefile</span>? 