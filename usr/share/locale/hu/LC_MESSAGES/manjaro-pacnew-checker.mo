��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     �  A   �  (   �  %     *   @  �  k  5   f	  H   �	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>új pacnew/pacsave fájlok találhatók:</span>
ne felejtsd el:
tudnod kell, hogy milyen választásokat teszel. ha bizonytalan vagy, kérdezz a közösségi csatornáinkon, mint például a támogatói fórumunkon. Ne csinálj semmit Tartsa meg az eredeti fájlt és távolítsa el a $suffix fájlt. Pacnew/pacsave kezelése befejeződött. Most eltávolítjuk a $suffix fájlt. Cseréld le az eredetit a $suffix fájlal. Ez a program ingyenes szoftver, amelyet újraoszthat vagy módosíthat a gnu általános közönségi licenc feltételei szerint, amelyet a szabad szoftver alapítvány 3. verziója jelentetett meg.

Ez a program abban a reményben kerül terjesztésre, hogy hasznos lesz, de bármilyen garancia nélkül, még a piacképesség vagy egy adott célra való alkalmasságra vonatkozóan sem. További részletekért lásd a gnu általános közönségi licencet.

Szerző: Stefano Capitani
stefano@manjaro.org Megtekintés és összefésülés a $suffix fájlban. Mit szeretnél tenni a fájllal <span foreground='red'>$basefile</span>? 