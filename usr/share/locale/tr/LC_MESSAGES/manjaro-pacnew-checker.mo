��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �     �     �  <   �  %     H   D  (   �  �  �  /   �	  I   �	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Aşağıdaki metni en'den tr'ye çevir: <span foreground='red'>yeni pacnew/pacsave dosyaları bulundu:\n</span>
akılda tutulması gerekenler:\n
seçimlerinizin farkında olmalısınız. Eğer emin değilseniz, destek forumumuz gibi sosyal kanallarımızı kullanarak bilgi alabilirsiniz. Hiçbir şey yapma Orijinal dosyayı koruyun ve $suffix dosyasını kaldırın. Pacnew/pacsave yönetimi tamamlandı. Şimdi $suffix dosyasını <span foreground='red'>kaldıracağız</span> $suffix dosyasıyla orijinali değiştir Bu program ücretsiz yazılımdır, özgür yazılım vakfı tarafından yayınlanan gnu genel kamu lisansı şartları altında yeniden dağıtabilir ve/veya değiştirebilirsiniz.

Bu programın kullanışlı olacağı umuduyla dağıtılır, ancak herhangi bir garanti olmaksızın, hatta ticari amaçlara uygunluk veya belirli bir amaca uygunluk konusunda bile ima edilen garantiler olmadan. Daha fazla ayrıntı için gnu genel kamu lisansına bakın.

Yazar: Stefano Capitani
stefano@manjaro.org Görüntüle ve $suffix dosyasını birleştir. <span foreground='red'>$basefile</span> dosyasıyla ne yapmak istersiniz? 