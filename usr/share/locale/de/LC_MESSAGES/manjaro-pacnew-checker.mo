��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �  
   �  4   �  ,   �  ,   	  -   6    d  .   	  N   �	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Neue pacnew/pacsave Dateien gefunden:\n</span>
Beachten Sie:\n
Sie müssen sich Ihrer Entscheidungen bewusst sein. Wenn Sie unsicher sind, fragen Sie bitte über unsere sozialen Kanäle wie unser Support-Forum nach. Nichts tun Behalte das Original und entferne die $suffix Datei. Verwaltung von pacnew/pacsave abgeschlossen. Jetzt werden wir die $suffix Datei entfernen Ersetze das Original durch die $suffix Datei. Dieses Programm ist freie Software, Sie können es weiterverteilen und/oder modifizieren unter den Bedingungen der GNU General Public License, wie sie von der Free Software Foundation in Version 3 veröffentlicht wurde.

Dieses Programm wird in der Hoffnung verteilt, dass es nützlich sein wird, aber ohne jegliche Garantie, ohne sogar die implizite Garantie der Marktgängigkeit oder Eignung für einen bestimmten Zweck. Sehen Sie sich die GNU General Public License für weitere Details an.

Autor: Stefano Capitani
stefano@manjaro.org Anzeigen und Zusammenführen der Datei $suffix Was möchtest du mit der Datei <span foreground='red'>$basefile</span> machen? 