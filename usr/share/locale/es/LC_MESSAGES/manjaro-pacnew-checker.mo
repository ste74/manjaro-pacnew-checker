��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     �  1   �  #   �  H     -   L  �  z  !   m	  L   �	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>nuevos archivos pacnew/pacsave encontrados:\n</span>
ten en cuenta:\n
debes estar consciente de tus opciones. si no estás seguro, por favor pregunta a través de nuestros canales sociales como nuestro foro de soporte. No hagas nada Mantén el original y elimina el archivo $suffix. Gestión de pacnew/pacsave completa Ahora vamos a <span foreground='red'>eliminar</span> el archivo $suffix. Reemplaza el original con el archivo $suffix. Este programa es software libre, puedes redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU publicada por la Fundación de Software Libre en su versión 3.

Este programa se distribuye con la esperanza de que sea útil, pero sin ninguna garantía, incluso sin la garantía implícita de comerciabilidad o aptitud para un propósito particular. Consulta la Licencia Pública General de GNU para obtener más detalles.

Autor: Stefano Capitani
stefano@manjaro.org Ver y fusionar el archivo $suffix ¿Qué quieres hacer con el archivo <span foreground='red'>$basefile</span>? 