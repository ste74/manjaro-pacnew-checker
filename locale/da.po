# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the manjaro-pacnew-checker package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: manjaro-pacnew-checker\n"
"Report-Msgid-Bugs-To: \n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: attranslate\n"

msgid "Now we will <span foreground='red'>remove</span> the $suffix file"
msgstr "Nu vil vi <span foreground='red'>fjerne</span> $suffix filen"

msgid ""
"This program is free software, you can redistribute it and/or modify it under the terms of the GNU "
"General Public License as published by the Free Software Foundation version 3 of the License.\n"
"\n"
"This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without "
"even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU "
"General Public License for more details.\n"
"\n"
"Author: Stefano Capitani\n"
"stefano@manjaro.org"
msgstr ""
"Dette program er gratis software, du kan redistribuere det og/eller ændre det under betingelserne i "
"GNU General Public License, som er udgivet af Free Software Foundation i version 3 af licensen.\n"
"\n"
"Dette program distribueres i håb om, at det vil være nyttigt, men uden nogen form for garanti, uden "
"endda den underforståede garanti for salgbarhed eller egnethed til et bestemt formål. Se GNU "
"General Public License for flere detaljer.\n"
"\n"
"Forfatter: Stefano Capitani\n"
"stefano@manjaro.org"

msgid ""
"<span foreground='red'>New Pacnew/Pacsave files found:\\n"
"</span>\n"
"Keep in mind:\\n"
"\n"
"You must be aware of your choices. If you are unsure please inquire using our social channels such "
"as our support forum.\\n"
msgstr ""
"<span foreground='red'>nye pacnew/pacsave filer fundet:\\n"
"</span>\n"
"husk:\\n"
"\n"
"du skal være opmærksom på dine valg. hvis du er i tvivl, bedes du spørge via vores sociale kanaler, "
"såsom vores supportforum."

msgid "What do you want to do with the file <span foreground='red'>$basefile</span>?\\n"
msgstr "Hvad vil du gøre med filen <span foreground='red'>$basefile</span>?"

msgid "View and merge the $suffix file"
msgstr "Vis og sammenflet $suffix filen"

msgid "Keep the original and remove the $suffix file"
msgstr "Behold den oprindelige og fjern $suffix filen."

msgid "Replace the original with the $suffix file"
msgstr "Erstat den originale fil med filen $suffix."

msgid "Do nothing"
msgstr "Gør ingenting"

msgid "Management of Pacnew/Pacsave complete"
msgstr "Håndtering af pacnew/pacsave fuldført"
