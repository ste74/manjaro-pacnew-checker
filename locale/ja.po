# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the manjaro-pacnew-checker package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: manjaro-pacnew-checker\n"
"Report-Msgid-Bugs-To: \n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: attranslate\n"

msgid "Now we will <span foreground='red'>remove</span> the $suffix file"
msgstr "今、私たちは$suffixファイルを<span foreground='red'>削除</span>します。"

msgid ""
"This program is free software, you can redistribute it and/or modify it under the terms of the GNU "
"General Public License as published by the Free Software Foundation version 3 of the License.\n"
"\n"
"This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without "
"even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU "
"General Public License for more details.\n"
"\n"
"Author: Stefano Capitani\n"
"stefano@manjaro.org"
msgstr ""
"このプログラムはフリーソフトウェアであり、フリーソフトウェア財団が公開したgnu一般公衆利用許諾書のバージョン3の条件の下で、再配布や改変が可能です。\n"
"\n"
"このプログラムは有用であることを願って配布されていますが、明示的な保証なしに、商業性や特定の目的への適合性も含めて、いかなる保証もありません。詳細についてはgnu一般公衆利用許諾書をご覧ください。\n"
"\n"
"著者：ステファノ・カピターニ\n"
"stefano@manjaro.org"

msgid ""
"<span foreground='red'>New Pacnew/Pacsave files found:\\n"
"</span>\n"
"Keep in mind:\\n"
"\n"
"You must be aware of your choices. If you are unsure please inquire using our social channels such "
"as our support forum.\\n"
msgstr ""
"<span foreground='red'>新しいpacnew/pacsaveファイルが見つかりました:\\n"
"</span>\n"
"注意してください:\\n"
"\n"
"あなたの選択を意識する必要があります。もし不安な場合は、サポートフォーラムなどのソーシャルチャンネルを使用してお問い合わせください。"

msgid "What do you want to do with the file <span foreground='red'>$basefile</span>?\\n"
msgstr "<span foreground='red'>$basefile</span>ファイルで何をしたいですか？"

msgid "View and merge the $suffix file"
msgstr "ファイル$suffixを表示してマージする"

msgid "Keep the original and remove the $suffix file"
msgstr "元のファイルを保持し、$suffixファイルを削除する"

msgid "Replace the original with the $suffix file"
msgstr "元のファイルを$suffixファイルで置き換える"

msgid "Do nothing"
msgstr "何もしない"

msgid "Management of Pacnew/Pacsave complete"
msgstr "pacnew/pacsaveの管理が完了しました。"
