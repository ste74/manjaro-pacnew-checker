#!/bin/bash

if [[ $1 == '-d' ]]; then
    /usr/bin/pacnew-checker -d
else /usr/bin/pacnew-checker
fi

[[ -f /tmp/pacnew-check.file ]] && \
/usr/bin/rm -f /tmp/pacnew-check.file

## Future reference for use polkit way

#cleanup() {
#  [ -f "/tmp/pacnew-check.file" ] && {
#      pkexec sh -c 'rm /tmp/pacnew-check.file'
#  }
#}

#trap cleanup EXIT